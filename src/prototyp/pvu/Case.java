package prototyp.pvu;

/**
 *
 * @author Simon & Gurra
 */
public class Case {

    private String location;
    private String type;
    private String informer;
    private String date;
    private String extraInformation;
    private String caseID;
    private int department;

    /**
     * Constructor
     *
     * @param location String
     * @param type String
     * @param informer String
     * @param date String
     * @param extraInformation String
     */
    public Case(String location, String type, String informer, String date, String extraInformation) {
        this.location = location;
        this.type = type;
        this.informer = informer;
        this.date = date;
        this.extraInformation = extraInformation;
        department = -1;
    }

    /**
     * get
     *
     * @return String
     */
    public String getLocation() {
        return location;
    }

    /**
     * set
     *
     * @param location String
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * get
     *
     * @return String
     */
    public String getType() {
        return type;
    }

    /**
     * set
     *
     * @param type String
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * get
     *
     * @return String
     */
    public String getInformer() {
        return informer;
    }

    /**
     * set
     *
     * @param informer String
     */
    public void setInformer(String informer) {
        this.informer = informer;
    }

    /**
     * get
     *
     * @return String
     */
    public String getDate() {
        return date;
    }

    /**
     * set
     *
     * @param date String
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * get
     *
     * @return String
     */
    public String getExtraInformation() {
        return extraInformation;
    }

    /**
     * set
     *
     * @param extraInformation String
     */
    public void setExtraInformation(String extraInformation) {
        this.extraInformation = extraInformation;
    }

    /**
     * get
     *
     * @return String
     */
    public String getCaseID() {
        return caseID;
    }

    /**
     * set
     *
     * @param caseID String
     */
    public void setCaseID(String caseID) {
        this.caseID = caseID;
    }

    /**
     * get
     *
     * @return int
     */
    public int getDepartment() {
        return department;
    }

    /**
     * set
     *
     * @param department int
     */
    public void setDepartment(int department) {
        this.department = department;
    }

}
