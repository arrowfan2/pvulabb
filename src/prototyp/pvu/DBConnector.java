package prototyp.pvu;

import java.util.ArrayList;

/**
 *
 * @author Simon
 */
public class DBConnector {

    public static DBConnector INSTANCE = new DBConnector();
    private String db;
    private String dbPassword;

    //hårdkodade grejer
    private int[] count = new int[4];
    private int counter = 0;
    ArrayList<Case> allCases = new ArrayList<Case>();

    private DBConnector() {
    }

    public static DBConnector getInstance() {
        return INSTANCE;
    }
    
    /**
     * checks if username matches password
     * @param username String   
     * @param password String
     * @return int answer
     */
    public int login(String username, String password) {
        String[] anv1 = {"berit", "12345"};
        String[] anv2 = {"sven", "23456"};
        String[] anv3 = {"sture", "34567"};
        String[] anv4 = {"GöranPetterson56", "12345"};
        String[] anv5 = {"ÅkeAndersson61", "67890"};
        String[][] uppgifter = {anv1, anv2, anv3,anv4,anv5};
        int answer = 0;

        for (String[] uppgifter1 : uppgifter) {
            if (uppgifter1[0].equals(username)) {
                if (uppgifter1[1].equals(password)) {
                    answer = 1;
                    break;
                } else {
                    answer = -1;
                    break;
                }
            } else {
                answer = -1;
            }
        }
        return answer;
    }
    
    /**
     * Registers a new case
     * @param fall Case
     * @return int answer
     */
    public String registerCase(Case fall) {
        Case tmp = fall;
        counter++;
        count[3] = counter / 1000;
        count[2] = (counter / 100) % 10;
        count[1] = (counter / 10) % 10;
        count[0] = counter % 10;
        tmp.setCaseID("2016-35-" + count[3] + count[2] + count[1] + count[0]);
        allCases.add(tmp);
        
        

        String answer = "2016-35-" + count[3] + count[2] + count[1] + count[0];
        return String.valueOf(answer);
    }

    /**
     * Get all saved cases
     * @param department int 
     * @param userID String
     * @return ArrayList<Case> 
     */
    public ArrayList<Case> showCases(int department, String userID) {
        return allCases;
    }
    
    /**
     * Edits a saved Case
     * @param caseID String
     * @param location String
     * @param type String
     * @param informer String 
     * @param date String
     * @param extraInformation String
     * @param department int
     * @return answer int 
     */
    public int editCase(String caseID, String location, String type, String informer, String date, String extraInformation, int department) {
        int answer = 0;
        for (Case allCase : allCases) {
            if (allCase.getCaseID().equals(caseID)) {
                allCase.setLocation(location);
                allCase.setType(type);
                allCase.setInformer(informer);
                allCase.setDate(date);
                allCase.setExtraInformation(extraInformation);
                allCase.setDepartment(department);
                answer = 1;
                break;
            } else {
                answer = -1;
            }
        }

        return answer;
    }

}
