package prototyp.pvu;

import java.util.ArrayList;

/**
 *
 * @author Gurra & Simon
 */
public class CaseHandler {
    private Case mCase;
    private ArrayList<Case> cases;

    /**
     * constructor
     */
    public CaseHandler() {
        createNewCase("Hemse", "eko", "sven göran : 070-00000", "2016-05-01", "Ph i marken");
        createNewCase("visby", "koldioxidutsläpp", "majbritt : 070-00000", "2016-05-02", "make röker för mycket ciggaretter");
        createNewCase("klintehamn", "miljöbrott", "åke : 070-00000", "2016-05-03", "impregneringsmedel ifrån träflis har sipprat ner i grundvatten");
        createNewCase("roma", "djurplågeri", "göran : 070-00000", "2016-05-04", "kor som dött");
    }
    
    /**
     * Create a new case
     * @param location String
     * @param type String
     * @param informer String
     * @param date String
     * @param extraInformation String
     * @return int answer
     */
    public String createNewCase(String location, String type, String informer, String date, String extraInformation){
        mCase = new Case(location, type, informer, date, extraInformation);
        String answer = DBConnector.getInstance().registerCase(mCase);
        return answer;
    }
    
    /**
     * Show a single case
     * @param CaseID
     * @return Case
     */
    public Case showCase(String CaseID){
        Case fall = null;
        for (Case aCase : cases) {
            if(aCase.getCaseID().equals(CaseID)){
                fall = aCase;
                break;
            }
        }
        return fall;
    }
    
    /**
     * Get all Cases
     * @param department int
     * @param userID String
     * @return ArrayList<Case> 
     */
    public ArrayList<Case> showCases(int department, String userID){
        cases = DBConnector.getInstance().showCases(department, userID);
        return cases;
    }    
}
